﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ClassLibrary1;

namespace oop14
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void AddPath(string path)
        {
            ListofFiles.Items.Add(path);
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            OpenFileDialog files = new OpenFileDialog();
            files.Multiselect = true;
            if (files.ShowDialog() == DialogResult.OK)
            {
                foreach (string file in files.FileNames)
                {
                    AddPath(file);
                }
            }
        }
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            ListofFiles.Items.Clear();
        }

        private void ListofFiles_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            int i;
            for (i = 0; i < files.Length; i++)
                ListofFiles.Items.Add(files[i]);
        }
        private void ListofFiles_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;
        }
        int yourchoice;

        private void шифруванняToolStripMenuItem_Click(object sender, EventArgs e)
        {
            yourchoice = 1;
        }

        private void розшифруванняToolStripMenuItem_Click(object sender, EventArgs e)
        {
            yourchoice = 2;
        }

        private void архівуванняToolStripMenuItem_Click(object sender, EventArgs e)
        {
            yourchoice = 3;
        }

        private void розархівуванняToolStripMenuItem_Click(object sender, EventArgs e)
        {
            yourchoice = 4;
        }

        private void buttonComplete_Click(object sender, EventArgs e)
        {
            string[] list = new string[ListofFiles.Items.Count];

            switch (yourchoice)
            {
                case 3:
                    for (int i = 0; i < ListofFiles.Items.Count; i++)
                    {
                        list[i] = ListofFiles.Items[i].ToString();
                    }
                    Archive.Zipping(list);
                    MessageBox.Show("Відбулось архівування !");
                    ListofFiles.Items.Clear();
                    break;
                case 4:
                    for (int i = 0; i < ListofFiles.Items.Count; i++)
                    {
                        list[i] = ListofFiles.Items[i].ToString();
                    }
                    Archive.Unzipping(list);
                    MessageBox.Show("Відбулось розархівування !");
                    ListofFiles.Items.Clear();
                    break;
                case 1:
                    byte[] listb = File.ReadAllBytes(ListofFiles.Items[0].ToString());
                    for (int i = 0; i < ListofFiles.Items.Count; i++)
                    {
                        string tmp = Encryption.Check((string)ListofFiles.Items[i]);
                        if (tmp != (string)ListofFiles.Items[i])
                        {
                            listb = File.ReadAllBytes((string)ListofFiles.Items[i]);
                            byte[] encrypt = Encryption.FileCrypt(listb);
                            string listName =
                            Encryption.Check((string)ListofFiles.Items[i]);
                            File.WriteAllBytes(listName, encrypt);
                            if (listName != ListofFiles.Items[i].ToString())
                            {
                                File.Delete(ListofFiles.Items[i].ToString());
                            }
                            MessageBox.Show("Відбулось шифрування !");
                        }
                    }
                    ListofFiles.Items.Clear();
                    break;
                case 2:
                    for (int i = 0; i < ListofFiles.Items.Count; i++)
                    {
                        string tmp =
                       Encryption.Check(ListofFiles.Items[i].ToString());
                        if (tmp != ListofFiles.Items[i].ToString())
                        {
                            listb = File.ReadAllBytes((string)ListofFiles.Items[i]);
                            byte[] encrypt = Encryption.FileCrypt(listb);
                            string listName =
                            Encryption.Check((string)ListofFiles.Items[i]);
                            File.WriteAllBytes(listName, encrypt);
                            if (listName != (string)ListofFiles.Items[i])
                            {
                                File.Delete(ListofFiles.Items[i].ToString());
                            }
                        }
                        MessageBox.Show("Відбулось розшифрування !");
                    }
                    ListofFiles.Items.Clear();
                    break;
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }
    }
}