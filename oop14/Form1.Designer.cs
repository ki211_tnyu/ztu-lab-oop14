﻿namespace oop14
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.ListofFiles = new System.Windows.Forms.ListBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.оберітьДіюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.шифруванняToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.розшифруванняToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.архівуванняToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.розархівуванняToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonComplete = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ListofFiles
            // 
            this.ListofFiles.FormattingEnabled = true;
            this.ListofFiles.Location = new System.Drawing.Point(162, 50);
            this.ListofFiles.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ListofFiles.Name = "ListofFiles";
            this.ListofFiles.Size = new System.Drawing.Size(283, 199);
            this.ListofFiles.TabIndex = 0;
            this.ListofFiles.DragDrop += new System.Windows.Forms.DragEventHandler(this.ListofFiles_DragDrop);
            this.ListofFiles.DragEnter += new System.Windows.Forms.DragEventHandler(this.ListofFiles_DragEnter);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // buttonAdd
            // 
            this.buttonAdd.BackColor = System.Drawing.Color.Black;
            this.buttonAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonAdd.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAdd.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonAdd.Location = new System.Drawing.Point(162, 268);
            this.buttonAdd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(106, 24);
            this.buttonAdd.TabIndex = 1;
            this.buttonAdd.Text = "Додати файл";
            this.buttonAdd.UseVisualStyleBackColor = false;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.BackColor = System.Drawing.Color.Black;
            this.buttonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonDelete.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonDelete.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonDelete.Location = new System.Drawing.Point(267, 310);
            this.buttonDelete.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(70, 24);
            this.buttonDelete.TabIndex = 2;
            this.buttonDelete.Text = "Видалити";
            this.buttonDelete.UseVisualStyleBackColor = false;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.оберітьДіюToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(602, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // оберітьДіюToolStripMenuItem
            // 
            this.оберітьДіюToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.шифруванняToolStripMenuItem,
            this.розшифруванняToolStripMenuItem,
            this.архівуванняToolStripMenuItem,
            this.розархівуванняToolStripMenuItem});
            this.оберітьДіюToolStripMenuItem.Name = "оберітьДіюToolStripMenuItem";
            this.оберітьДіюToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.оберітьДіюToolStripMenuItem.Text = "Оберіть дію";
            // 
            // шифруванняToolStripMenuItem
            // 
            this.шифруванняToolStripMenuItem.Name = "шифруванняToolStripMenuItem";
            this.шифруванняToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.шифруванняToolStripMenuItem.Text = "шифрування";
            this.шифруванняToolStripMenuItem.Click += new System.EventHandler(this.шифруванняToolStripMenuItem_Click);
            // 
            // розшифруванняToolStripMenuItem
            // 
            this.розшифруванняToolStripMenuItem.Name = "розшифруванняToolStripMenuItem";
            this.розшифруванняToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.розшифруванняToolStripMenuItem.Text = "розшифрування";
            this.розшифруванняToolStripMenuItem.Click += new System.EventHandler(this.розшифруванняToolStripMenuItem_Click);
            // 
            // архівуванняToolStripMenuItem
            // 
            this.архівуванняToolStripMenuItem.Name = "архівуванняToolStripMenuItem";
            this.архівуванняToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.архівуванняToolStripMenuItem.Text = "архівування";
            this.архівуванняToolStripMenuItem.Click += new System.EventHandler(this.архівуванняToolStripMenuItem_Click);
            // 
            // розархівуванняToolStripMenuItem
            // 
            this.розархівуванняToolStripMenuItem.Name = "розархівуванняToolStripMenuItem";
            this.розархівуванняToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.розархівуванняToolStripMenuItem.Text = "розархівування";
            this.розархівуванняToolStripMenuItem.Click += new System.EventHandler(this.розархівуванняToolStripMenuItem_Click);
            // 
            // buttonComplete
            // 
            this.buttonComplete.BackColor = System.Drawing.Color.Black;
            this.buttonComplete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonComplete.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonComplete.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonComplete.Location = new System.Drawing.Point(334, 268);
            this.buttonComplete.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonComplete.Name = "buttonComplete";
            this.buttonComplete.Size = new System.Drawing.Size(110, 24);
            this.buttonComplete.TabIndex = 4;
            this.buttonComplete.Text = "Виконати дію";
            this.buttonComplete.UseVisualStyleBackColor = false;
            this.buttonComplete.Click += new System.EventHandler(this.buttonComplete_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.linkLabel1.Location = new System.Drawing.Point(452, 345);
            this.linkLabel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(117, 13);
            this.linkLabel1.TabIndex = 5;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Тишкевич Н.Ю КІ-21-1";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(602, 372);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.buttonComplete);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.ListofFiles);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximumSize = new System.Drawing.Size(618, 411);
            this.MinimumSize = new System.Drawing.Size(618, 411);
            this.Name = "Form1";
            this.Text = "Операції над файлами";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox ListofFiles;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem оберітьДіюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem шифруванняToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem розшифруванняToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem архівуванняToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem розархівуванняToolStripMenuItem;
        private System.Windows.Forms.Button buttonComplete;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}

