﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Windows;

namespace ClassLibrary1
{
    public class Archive
    {
        public static void Zipping(string[] list)
        {
            string zip = list[0];
            if (File.Exists(zip + ".gzar"))
            {
            }
            else
            {
                string tempzip = Path.GetDirectoryName(list[0]) + "\\Zip";
                Directory.CreateDirectory(tempzip);
                for (int i = 0; i < list.Length; i++)
                {
                    File.Copy(list[i], tempzip + "\\" + Path.GetFileName(list[i]),
                   true);
                }
                ZipFile.CreateFromDirectory(tempzip, zip + ".gzar");
                Directory.Delete(tempzip, true);
            }
        }
        public static void Unzipping(string[] list)
        {
            for (int i = 0; i < list.Length; i++)
            {
                string zipD = Path.GetFullPath(list[i]);
                string directoryF = Path.GetDirectoryName(zipD);
                Directory.CreateDirectory(directoryF);
                ZipFile.ExtractToDirectory(zipD, directoryF);
            }
        }
    }
}
