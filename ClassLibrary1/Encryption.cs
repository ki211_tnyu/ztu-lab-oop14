﻿using System;
using System.Collections.Generic;
using System.Text;
namespace ClassLibrary1
{
    public class Encryption
    {
        public static byte[] FileCrypt(byte[] bytes)
        {
            for (int i = 0; i < bytes.Length; i++)
                bytes[i] ^= 1;
            return bytes;
        }
        public static string Check(string fileName)
        {
            return fileName.EndsWith(".crypt") ?
           fileName.Remove(fileName.LastIndexOf(".crypt")) : fileName + ".crypt";
        }
    }
}